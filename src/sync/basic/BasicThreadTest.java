package sync.basic;

public class BasicThreadTest {

    public static void main(String[] args) {
        ThreadA threadA = new ThreadA();
        ThreadB threadB = new ThreadB();
//        threadA.start();
//        threadB.start();

        CountThread countThread = new CountThread();
        countThread.start();

        try {
            countThread.join();
            System.out.println(countThread.getResult());
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
